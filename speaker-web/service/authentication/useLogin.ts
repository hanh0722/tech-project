import { useMutation } from "react-query";
import { ILoginUserInput, ILoginUserResponse } from "../../types/services/authentication";
import { makeRequest } from "../base";
import { gql } from "graphql-request";

const loginMutation = gql`
  mutation loginUser($input: LoginUserInput!) {
    loginUser(input: $input) {
      message
      code
      token
      data {
        id
        email
        phoneNumber
        status
        userId
      }
    }
  }
`;
const useLogin = () => {
  return useMutation({
    mutationFn: async (input: ILoginUserInput) => {
      const response = await makeRequest<ILoginUserResponse>(loginMutation, {
        input
      });
      return response?.loginUser;
    }
  })
};

export default useLogin;
