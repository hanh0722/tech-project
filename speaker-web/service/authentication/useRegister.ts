import { useMutation } from "react-query";
import { makeRequest } from "../base";
import { IRegisterUserInput, IRegisterUserResponse } from "../../types/services/authentication";
import { gql } from "graphql-request";

const registerMutation = gql`
  mutation register($input: RegisterUserInput!) {
    register(input: $input) {
      message
      code
    }
  }
`;

const useRegister = () => {
  return useMutation({
    mutationFn: async (input: IRegisterUserInput) => {
      const response = await makeRequest<IRegisterUserResponse>(registerMutation, {
        input
      })
      return response?.register;
    }
  })
};

export default useRegister;
