import { useMutation } from "react-query";
import { UpdateUserProfileInput } from "../types/request";
import { request } from "./class/auth";

const USER_API = "/api/user";
export const useUpdateUserProfile = () => {
  return useMutation({
    mutationFn: async (input: UpdateUserProfileInput) => {
      const response = await request.put(`${USER_API}/update`, input);
      return response?.data;
    },
  });
};
