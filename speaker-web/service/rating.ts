import { useMutation, useQuery } from "react-query";
import { request } from "./class/auth";
import { CreateReviewInput, CreateReviewResponse, GetListReviewsInput, GetListReviewsResponse } from "../types/request";

const RATING_URL = "/api/rating";
export const useCreateRating = () => {
  return useMutation({
    mutationFn: async (input: CreateReviewInput) => {
      const response = await request.post<CreateReviewResponse>(
        `${RATING_URL}/create`,
        input
      );
      return response?.data;
    },
  });
};

export const useGetRating = (input: GetListReviewsInput) => {
  const { productId, ...rest } = input;
  return useQuery({
    queryKey: ['rating', productId, rest],
    queryFn: async () => {
      const response = await request.get<GetListReviewsResponse>(`${RATING_URL}/list/${productId}`, {
        params: rest
      });
      return response?.data;
    }
  })
}