import request, {
  GraphQLClient,
  RequestDocument,
  Variables,
} from "graphql-request";
import { GRAPHQL_URL } from "../utils/config";
import { getCookie } from "../utils/cookies";

const client = new GraphQLClient(GRAPHQL_URL);

export const makeRequest = <T extends unknown>(
  document: string,
  variables?: Variables
) => {
  const token = getCookie("token");
  return new Promise<T>((resolve, reject) => {
    client
      .request<T>(document, variables, {
        Authorization: `Bearer ${token ?? ""}`,
      })
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        if (err instanceof Error) {
          const errors =
            (
              err as unknown as {
                response: { errors: Array<{ message: string }> };
              }
            ).response.errors[0].message ?? "";
          reject(new Error(errors));
        }
      });
  });
};
