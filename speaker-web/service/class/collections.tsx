import { useQuery } from "react-query";
import { CorePaginationParams } from "../../types/api/core";
import { BASE_URL } from "../../utils/config";
import { request } from "./auth";

const CORE_API = `${BASE_URL}/api/collections`;

export const getCollections = (params?: CorePaginationParams) => {
  return request.get(`${CORE_API}/get`, {
    params: {
      ...(params || {})
    }
  })
};

export const useCollections = (input?: CorePaginationParams) => {
  return useQuery({
    queryKey: ['collections', input],
    queryFn: async () => {
      const response = await getCollections(input);
      return response?.data;
    }
  })
}