import { useCallback } from "react";
import { PAGE_DEFAULT, PER_PAGE_DEFAULT } from "../constants/sort";
import useFetch from "../hook/useFetch";
import {
  BaseSortRequest,
  CollectionResponse,
  GetListCollectionsResponse,
  GetProductsByCollectionInput,
} from "../types/request";
import { BASE_URL } from "../utils/config";
import { useQuery } from "react-query";
import { request } from "./class/auth";
import { DEFAULT_PAGE_SIZE } from "../constants/products";

const BASE_COLLECTION_URL = `${BASE_URL}/api/collections`;

export const useCollections = () => {
  const { isLoading, data, error, request, onResetAsync } =
    useFetch<CollectionResponse>();
  const onFetchCollections = useCallback(
    async ({
      sort,
      key,
      page_size = PER_PAGE_DEFAULT,
      page = PAGE_DEFAULT,
    }: BaseSortRequest) => {
      request({
        url: `${BASE_COLLECTION_URL}/get`,
        params: {
          sort,
          key,
          page_size,
          page,
        },
      });
    },
    [request]
  );
  return {
    isLoading,
    data,
    error,
    onFetchCollections,
    onResetAsync,
  };
};

export const useCollectionHandler = (
  input?: BaseSortRequest
) => {
  const inputQuery = input ?? {page: PAGE_DEFAULT, page_size: DEFAULT_PAGE_SIZE}
  return useQuery({
    queryKey: ["get_collections", inputQuery],
    queryFn: async () => {
      const response = await request.get<GetListCollectionsResponse>(
        `${BASE_COLLECTION_URL}/get`,
        {
          params: input,
        }
      );
      return response?.data;
    },
  });
};

export const useProductsByCollection = ({ collectionId, ...rest }: GetProductsByCollectionInput) => {
  return useQuery({
    queryKey: ['get_product_by_collection', collectionId, rest],
    queryFn: async () => {
      const response = await request.get(`${BASE_COLLECTION_URL}/${collectionId}/products`, {
        params: rest
      })
      return response.data;
    }
  })
}