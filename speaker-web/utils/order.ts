import { OrderItem } from "../types/api/page/orders";

export const getTotalOrder = (items: OrderItem[]) => {
  return items.reduce((acc, item) => {
      return acc + item.quantity * item.price;
  }, 0);
}