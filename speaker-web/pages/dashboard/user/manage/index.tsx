import { HeadGeneral } from "../../../../components/common";
import ListManageUser from "../../../../components/container/Dashboard/ManageUser/ListManageUser";
import BreadCrumbDirection from "../../../../components/core/BreadCrumbDirection";
import { DashboardLayout } from "../../../../components/layout";
import { DASH_BOARD, MANAGE_USER } from "../../../../constants/path";
import { NextPageWithLayout } from "../../../../types/layout";
import styles from "./styles.module.scss";

const ManageUser: NextPageWithLayout = () => {
  return (
    <>
      <HeadGeneral title={`Manage user`} />
      <BreadCrumbDirection className={styles.breadcrumb}>
        <BreadCrumbDirection.Element href={DASH_BOARD}>
          Dashboard
        </BreadCrumbDirection.Element>
        <BreadCrumbDirection.Element showCaret={false} href={MANAGE_USER}>
          Manage user
        </BreadCrumbDirection.Element>
      </BreadCrumbDirection>
      <ListManageUser/>
    </>
  );
};

ManageUser.getLayout = function (page) {
  return <DashboardLayout className={styles.layout}>{page}</DashboardLayout>;
};

export default ManageUser;
