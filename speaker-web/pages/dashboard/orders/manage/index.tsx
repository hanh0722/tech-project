import { useEffect, useState } from "react";
import { Pagination, Table } from "../../../../components/core";
import BreadCrumbDirection from "../../../../components/core/BreadCrumbDirection";
import { DashboardLayout } from "../../../../components/layout";
import { MANAGE_ORDERS, VIEW_DETAIL_ORDER } from "../../../../constants/path";
import useFetch from "../../../../hook/useFetch";
import { NextPageWithLayout } from "../../../../types/layout";
import styles from "./styles.module.scss";
import { BASE_URL } from "../../../../utils/config";
import LoadingTable from "../../../../components/common/LoadingTable";
import { isArray } from "../../../../utils/array";
import { getTotalOrder } from "../../../../utils/order";
import moment from "moment";
import { useRouter } from "next/router";
import { Button, Chip } from "@mui/material";
import { Menu } from "../../../../components/core";
import { IconEye, IconThreeDot } from "../../../../components/core/Icons";
import MenuItem from "../../../../components/core/Menu/MenuItem";
import IconEdit from "../../../../components/core/Icons/IconEdit";

const ManageOrders: NextPageWithLayout = () => {
  const { request, isLoading, data } = useFetch();
  const [page, setPage] = useState(1);
  const { query, push } = useRouter();

  useEffect(() => {
    request({
      url: `${BASE_URL}/api/orders/list`,
      method: "post",
      data: {
        page,
        pageSize: 10,
      },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  useEffect(() => {
    const pageData = query?.page;
    if (!pageData) {
      setPage(1);
    } else {
      setPage(+pageData);
    }
  }, [query]);

  const onViewDetail = (id: string) => {
    push(VIEW_DETAIL_ORDER(id));
  }

  return (
    <>
      <div className={styles.container}>
        <BreadCrumbDirection className={styles.flex}>
          <BreadCrumbDirection.Element href="/">
            Home
          </BreadCrumbDirection.Element>
          <BreadCrumbDirection.Element href={MANAGE_ORDERS}>
            Manage Orders
          </BreadCrumbDirection.Element>
        </BreadCrumbDirection>
        <div className={styles.table}>
          <Table>
            <Table.Head>
              <Table.Row>
                <Table.Cell align="start">ID</Table.Cell>
                <Table.Cell align="start">Name</Table.Cell>
                <Table.Cell>Payment Method</Table.Cell>
                <Table.Cell>Total</Table.Cell>
                <Table.Cell>Status</Table.Cell>
                <Table.Cell align="start">Created At</Table.Cell>
                <Table.Cell></Table.Cell>
              </Table.Row>
            </Table.Head>
            <Table.Body>
              {isLoading && <LoadingTable cols={6} />}
              {!isLoading &&
                isArray(data?.data?.data) &&
                data?.data?.data?.map((item: any) => {
                  return (
                    <Table.Row key={item._id}>
                      <Table.Cell align="start">{item?._id}</Table.Cell>
                      <Table.Cell align="start">
                        {item?.object_id?.name}
                      </Table.Cell>
                      <Table.Cell>
                        <Chip label={item?.payment_methods} variant="filled" />
                      </Table.Cell>
                      <Table.Cell>
                        ${getTotalOrder(item?.items ?? [])}
                      </Table.Cell>
                      <Table.Cell>{item?.status}</Table.Cell>
                      <Table.Cell align="start">
                        {moment(item?.creation_time).format("DD/MM/YYYY")}
                      </Table.Cell>
                      <Table.Cell align="center">
                        <Menu
                          trigger={
                            <Button
                              variant="text"
                              prefix="normal"
                              color="inherit"
                            >
                              <IconThreeDot />
                            </Button>
                          }
                        >
                          <MenuItem onClick={() => onViewDetail(item._id)}>
                            <IconEye/> View detail
                          </MenuItem>
                          <MenuItem>
                            <IconEdit/> Edit
                          </MenuItem>
                        </Menu>
                      </Table.Cell>
                    </Table.Row>
                  );
                })}
            </Table.Body>
          </Table>
          {data?.data?.total && (
            <Pagination totalItems={data?.data?.total ?? 0} itemPerPage={10} />
          )}
        </div>
      </div>
    </>
  );
};

ManageOrders.getLayout = function (page) {
  return <DashboardLayout>{page}</DashboardLayout>;
};

export default ManageOrders;
