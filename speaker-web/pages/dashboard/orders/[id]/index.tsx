import { GetServerSideProps } from "next";
import { DashboardLayout } from "../../../../components/layout";
import { NextPageWithLayout } from "../../../../types/layout";
import { request } from "../../../../service/class/auth";
import { BASE_URL } from "../../../../utils/config";
import { Avatar, Box, Chip, Grid, Typography } from "@mui/material";
import { HeadGeneral } from "../../../../components/common";
import BreadCrumbDirection from "../../../../components/core/BreadCrumbDirection";
import {
  DASH_BOARD,
  ORDER_LIST,
  VIEW_DETAIL_ORDER,
} from "../../../../constants/path";
import styles from "./styles.module.scss";
import { getTotalOrder } from "../../../../utils/order";
import moment from "moment";

const OrderDetail: NextPageWithLayout = ({ order }: any) => {
  const data = order?.data;
  const info = data?.object_info_id?.info;
  const items = data?.items;
  console.log(order);
  return (
    <>
      <HeadGeneral title={"Order detail"} />
      <Box sx={{ paddingLeft: "32px", paddingRight: "32px" }}>
        <BreadCrumbDirection className={styles.breadcrumb}>
          <BreadCrumbDirection.Element href={DASH_BOARD}>
            Dashboard
          </BreadCrumbDirection.Element>
          <BreadCrumbDirection.Element href={ORDER_LIST}>
            Orders
          </BreadCrumbDirection.Element>
          <BreadCrumbDirection.Element
            href={VIEW_DETAIL_ORDER(data?._id)}
            showCaret={false}
          >
            Detail order
          </BreadCrumbDirection.Element>
        </BreadCrumbDirection>
        <Grid spacing={5} container>
          <Grid item xs={6}>
            <Typography
              variant="h5"
              sx={{ fontWeight: "bold", paddingBottom: "20px" }}
            >
              General information
            </Typography>
            <Box sx={stylesObj.item}>
              <Typography sx={{ fontWeight: "600", paddingBottom: "6px" }}>
                Order ID
              </Typography>
              <Typography>{data?._id}</Typography>
            </Box>
            <Box sx={stylesObj.item}>
              <Typography sx={{ fontWeight: "600", paddingBottom: "6px" }}>
                User name
              </Typography>
              <Typography>{data?.object_id?.name}</Typography>
            </Box>
            <Box sx={stylesObj.item}>
              <Typography sx={{ fontWeight: "600", paddingBottom: "6px" }}>
                Name
              </Typography>
              <Typography>{info?.full_name}</Typography>
            </Box>
            <Box sx={stylesObj.item}>
              <Typography sx={{ fontWeight: "600", paddingBottom: "6px" }}>
                Address
              </Typography>
              <Typography>
                {info?.place}, {info?.address}, {info?.city}, {info?.country}
              </Typography>
            </Box>
            <Box sx={stylesObj.item}>
              <Typography sx={{ fontWeight: "600", paddingBottom: "6px" }}>
                Phone number
              </Typography>
              <Typography>{info?.phone_number}</Typography>
            </Box>
            <Box sx={stylesObj.item}>
              <Typography sx={{ fontWeight: "600", paddingBottom: "6px" }}>
                Zip code
              </Typography>
              <Typography>{info?.zip_code}</Typography>
            </Box>
            <Box sx={stylesObj.item}>
              <Typography sx={{ fontWeight: "600", paddingBottom: "6px" }}>
                Payment method
              </Typography>
              <Chip label={data?.payment_methods} />
            </Box>
            <Box sx={stylesObj.item}>
              <Typography sx={{ fontWeight: "600", paddingBottom: "6px" }}>
                Status
              </Typography>
              <Chip
                label={data?.status?.toLowerCase()}
                sx={{ "& .MuiChip-label": { textTransform: "capitalize" } }}
              />
            </Box>
            <Box sx={stylesObj.item}>
              <Typography sx={{ fontWeight: "600", paddingBottom: "6px" }}>
                Delivery method
              </Typography>
              <Chip
                label={data?.delivery_methods?.toLowerCase()}
                sx={{ "& .MuiChip-label": { textTransform: "capitalize" } }}
              />
            </Box>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="h5"
              sx={{ fontWeight: "bold", paddingBottom: "20px" }}
            >
              Order items
            </Typography>
            {(items ?? []).map((item) => {
              return (
                <Box
                  key={item?._id}
                  sx={{
                    paddingBottom: "20px",
                    marginBottom: "20px",
                    borderBottom: "1px solid #ddd",
                    display: "flex",
                  }}
                >
                  <Avatar
                    sx={{ width: "80px", height: "80px" }}
                    src={item?.images?.[0]}
                  />
                  <Box>
                    <Typography sx={{ fontWeight: "600", fontSize: '18px' }}>
                      {item?.title}{" "}
                      <Typography
                        sx={{ color: "#8590a6", fontWeight: "600", }}
                        component="span"
                      >
                        x{item?.quantity}
                      </Typography>
                    </Typography>
                    <Typography sx={{ color: "#8590a6", paddingTop: '4px', paddingBottom: '4px' }}>
                      Price: ${item?.price}
                    </Typography>
                    <Typography sx={{ color: "#8590a6" }}>
                      Total: ${item?.price * item?.quantity}
                    </Typography>
                  </Box>
                </Box>
              );
            })}
            <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'space-between'}}>
              <Typography sx={{fontWeight: '600'}}>Total:</Typography>
              <Typography>${getTotalOrder(items)}</Typography>
            </Box>
            <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'space-between', paddingTop: '16px'}}>
              <Typography sx={{fontWeight: '600'}}>Created at:</Typography>
              <Typography>{moment(data?.creation_time).format('DD/MM/YYYY hh:mm:ss')}</Typography>
            </Box>
            <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'space-between', paddingTop: '16px'}}>
              <Typography sx={{fontWeight: '600'}}>Paid by customer:</Typography>
              <Chip label={data?.is_paid ? 'Paid' : 'Not paid'}/>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

const stylesObj = {
  item: {
    paddingBottom: "20px",
  },
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { req, params = {} } = context;
  try {
    const id = params?.["id"];
    const cookie = req.cookies;
    const token = cookie?.token;
    if (!id || !token) {
      return {
        notFound: true,
      };
    }
    const orderDetail = await request.get(`${BASE_URL}/api/orders/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    console.log(orderDetail);
    return {
      props: {
        order: orderDetail.data,
      },
    };
  } catch (err) {
    return {
      notFound: true,
    };
  }
};
OrderDetail.getLayout = function (page) {
  return <DashboardLayout className={styles.layout}>{page}</DashboardLayout>;
};

export default OrderDetail;
