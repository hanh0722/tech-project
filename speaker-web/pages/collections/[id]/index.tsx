import { useRouter } from "next/router";
import { Container } from "../../../components/core";
import BreadCrumbDirection from "../../../components/core/BreadCrumbDirection";
import { MainLayout } from "../../../components/layout";
import { COLLECTION_DETAIl, HOME } from "../../../constants/path";
import { NextPageWithLayout } from "../../../types/layout";
import { GetServerSideProps } from "next";
import { request } from "../../../service/class/auth";
import { Collection } from "../../../types/api/page/collections";
import { useProductsByCollection } from "../../../service/collections";
import ListProducts from "../../../components/container/Products/ListProducts";

const CollectionDetail: NextPageWithLayout<{ collection: Collection }> = (
  props
) => {
  const { collection } = props;
  const { query } = useRouter();
  const collectionId = query?.id ?? "";
  const { data, isLoading } = useProductsByCollection({
    collectionId: collectionId.toString(),
  });
  return (
    <Container>
      <BreadCrumbDirection title="Collection">
        <BreadCrumbDirection.Element href={HOME}>
          Home
        </BreadCrumbDirection.Element>
        <BreadCrumbDirection.Element
          showCaret={false}
          href={COLLECTION_DETAIl(collectionId?.toString())}
        >
          {collection?.title}
        </BreadCrumbDirection.Element>
      </BreadCrumbDirection>
      <ListProducts
        rows={4}
        isLoading={isLoading}
        data={data?.data ?? []}
        totalProducts={data?.total ?? 0}
      />
    </Container>
  );
};

CollectionDetail.getLayout = function (page) {
  return <MainLayout>{page}</MainLayout>;
};

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const id = query?.id;
  if (!id) {
    return {
      notFound: true,
    };
  }
  try {
    const collection = await request.get(`/api/collections/${id}`);
    return {
      props: {
        collection: collection.data?.data,
      },
    };
  } catch (err) {
    return {
      notFound: true,
    };
  }
};
export default CollectionDetail;
