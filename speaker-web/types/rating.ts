import { User } from "./redux";

export interface RatingAttachment {
  _id: string;
  url: string;
  title: string;
}

export interface Rating {
  _id: string;
  description: string;
  rateNumber: number;
  title: string;
  createdAt: string;
  attachments: RatingAttachment[];
  user: User
}
