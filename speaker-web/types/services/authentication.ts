import { BaseResponse } from "../request";
import { IBaseResponse } from "./base";
import { IUser } from "./user";

export interface ILoginUserInput {
  email: string;
  password: string;
};

export interface ILoginUserResult extends IBaseResponse {
  data: IUser;
  token: string;
}

export interface ILoginUserResponse {
  loginUser: ILoginUserResult;
}

export interface IRegisterUserInput {
  email: string;
  password: string;
}

export interface IRegisterUserResult extends BaseResponse {}

export interface IRegisterUserResponse {
  register: IRegisterUserResult;
}