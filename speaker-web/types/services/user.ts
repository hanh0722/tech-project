export enum IUserStatus {
  ACTIVATED = 'activated',
  DEACTIVATED = 'deactivated',
  UNVERIFIED = 'unverified'
}

export interface IUser {
  id: number;
  email: string;
  phoneNumber: string;
  status: IUserStatus;
  userId: string;
}