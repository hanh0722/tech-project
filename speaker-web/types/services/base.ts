export interface IBaseResponse {
  message: string;
  code: number;
}