export interface Collection {
  image_url: string[];
  title: string;
  creation_time: number;
  seo_id: string;
  createdAt: string;
  updatedAt: string;
}
