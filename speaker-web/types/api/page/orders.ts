import { Collection } from "./collections";

export interface OrderItem {
  collections: Collection;
  description: string;
  images: string[];
  price: number;
  quantity: number;
  title: string;
  _id: string;
}