import { Box, MenuItem, Select, SelectProps, Skeleton } from "@mui/material";
import { useCollectionHandler } from "../../../service/collections";
import { generateArray } from "../../../utils/array";
import { FC } from "react";

interface SelectCategoryProps extends SelectProps {}
const SelectCategory: FC<SelectCategoryProps> = (props) => {
  const { data, isLoading } = useCollectionHandler();
  const items = data?.data ?? [];
  return (
    <Select {...props}>
      {isLoading && (
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            gap: "8px",
            padding: "12px",
          }}
        >
          {generateArray(5).map((item) => (
            <Skeleton key={item} width={"100%"} height={"32px"} />
          ))}
        </Box>
      )}
      {!isLoading &&
        items.map((item) => {
          return (
            <MenuItem key={item._id} value={item._id}>
              {item.title}
            </MenuItem>
          );
        })}
    </Select>
  );
};

export default SelectCategory;
