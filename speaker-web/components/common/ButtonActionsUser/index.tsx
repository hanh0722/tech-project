import {
  Box,
  Button,
  ClickAwayListener,
  Grow,
  MenuItem,
  MenuList,
  Paper,
  Popper,
  Typography,
} from "@mui/material";
import { IconClose, IconThreeDot } from "../../core/Icons";
import { MouseEvent, useState } from "react";

const ButtonActionsUser = () => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const open = !!anchorEl;
  const onHide = () => {
    setAnchorEl(null);
  };

  const onOpen = (event: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const onDeactivateAccount = () => {
    onHide();
  }
  return (
    <>
      <Button onClick={onOpen} variant="outlined" sx={{ minWidth: "auto" }}>
        <IconThreeDot />
      </Button>
      <Popper anchorEl={anchorEl} transition open={open}>
        {({ TransitionProps }) => {
          return (
            <Grow {...TransitionProps}>
              <Paper>
                <ClickAwayListener onClickAway={onHide}>
                  <MenuList autoFocusItem>
                    <MenuItem onClick={onDeactivateAccount}>
                      <Box sx={{display: 'flex', alignItems: 'center', gap: '8px'}}>
                        <IconClose style={{ fill: "rgb(100, 116, 139)", width: '14px', height: '14px' }} />
                        <Typography variant="body2">Deactivate account</Typography>
                      </Box>
                    </MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          );
        }}
      </Popper>
    </>
  );
};

export default ButtonActionsUser;
