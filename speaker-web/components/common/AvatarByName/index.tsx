import { Avatar, SxProps, Theme } from "@mui/material";

const stringToColor = (string: string) => {
  let hash = 0;
  let i;

  /* eslint-disable no-bitwise */
  for (i = 0; i < string.length; i += 1) {
    hash = string.charCodeAt(i) + ((hash << 5) - hash);
  }

  let color = '#';

  for (i = 0; i < 3; i += 1) {
    const value = (hash >> (i * 8)) & 0xff;
    color += `00${value.toString(16)}`.slice(-2);
  }
  /* eslint-enable no-bitwise */

  return color;
}

const stringAvatar = (name: string, sx?: SxProps<Theme> | undefined, displayTitle?: boolean) => {
  const arr = name.split(' ');
  return {
    sx: {
      bgcolor: stringToColor(name),
      ...sx
    },
    children: `${arr[0][0]}${arr.length > 1 ? arr[1][0] : (arr[0].length > 1 ? arr[0][1] : '')}`.toLocaleUpperCase(),
    title: (displayTitle ? name : undefined)
  };
}

interface AvatarByNameProps {
  name: string;
  sx?: SxProps<Theme> | undefined;
  displayTitle?: boolean;
}

const AvatarByName = ({ name, sx = {}, displayTitle }: AvatarByNameProps) => {
  return <Avatar {...stringAvatar(name, sx, displayTitle)} />;
};

export default AvatarByName;