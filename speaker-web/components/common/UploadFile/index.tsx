import React, { ChangeEvent, Component, HTMLAttributes, RefObject } from "react";
import { isFunction } from "../../../types/type";
import PropTypes from 'prop-types';

interface UploadFileProps extends HTMLAttributes<HTMLInputElement> {
  innerRef?: RefObject<HTMLInputElement>;
  multiple?: boolean;
  accepts?: string
}
interface UploadFileState {
  isLoading: boolean;
}
class UploadFile extends Component<UploadFileProps, UploadFileState> {
  inputRef: null | RefObject<HTMLInputElement> = null;

  static defaultProps = {
    multiple: false,
    accepts: '*'
  };

  static propTypes = {
    multiple: PropTypes.bool,
    accepts: PropTypes.string
  }
  constructor(props: UploadFileProps) {
    super(props);
    this.state = {
      isLoading: false,
    };
    this.inputRef = React.createRef<HTMLInputElement>();
  }

  onClickFile = () => {
    this.inputRef?.current?.click();
  };

  onGetFile = (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange } = this.props;
    if (isFunction(onChange)) {
      onChange(event);
    }
  }

  render(): React.ReactNode {
    const { accepts, multiple, ...restProps } = this.props;
    return (
      <>
        <input {...restProps} accept={accepts} multiple={multiple} style={{ display: 'none' }} type="file" ref={this.inputRef} onChange={this.onGetFile}/>
      </>
    );
  }
};

export default UploadFile
