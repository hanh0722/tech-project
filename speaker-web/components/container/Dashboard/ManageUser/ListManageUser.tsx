import { ChangeEvent, useRef, useState } from "react";
import { useSearchUserByQuery } from "../../../../service/user";
import { Pagination, Table } from "../../../core";
import LoadingTable from "../../../common/LoadingTable";
import { Avatar, Box, Chip, TextField, Typography } from "@mui/material";
import moment from "moment";
import { useRouter } from "next/router";
import AvatarByName from "../../../common/AvatarByName";
import ButtonActionsUser from "../../../common/ButtonActionsUser";

interface ListManageUserProps {}

const ListManageUser = () => {
  const router = useRouter();
  const { page = 1 } = router.query;
  const timeout = useRef<number | null>(null);
  const [query, setQuery] = useState("");
  const { data, isLoading } = useSearchUserByQuery(query, {
    page: +page,
  });
  const members = data?.data ?? [];
  const total = data?.total_users ?? 0;

  const onChangeKeyword = (event: ChangeEvent<HTMLInputElement>) => {
    if (timeout.current) {
      clearTimeout(timeout.current);
    }
    timeout.current = window.setTimeout(() => {
      setQuery(event.target.value);
    }, 300);
  };
  return (
    <Box
      sx={{
        padding: "16px",
        borderRadius: "4px",
        boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.12)",
      }}
    >
      <TextField
        onChange={onChangeKeyword}
        label="Search..."
        fullWidth
        sx={{ marginBottom: "16px" }}
      />
      <Table>
        <Table.Head>
          <Table.Row>
            <Table.Cell align="start">ID</Table.Cell>
            <Table.Cell align="start">Username</Table.Cell>
            <Table.Cell align="start">Name</Table.Cell>
            <Table.Cell align="start">Info</Table.Cell>
            <Table.Cell>Status</Table.Cell>
            <Table.Cell align="start">Created At</Table.Cell>
            <Table.Cell></Table.Cell>
          </Table.Row>
        </Table.Head>
        <Table.Body>
          {isLoading && <LoadingTable cols={6} />}
          {!isLoading &&
            members.length > 0 &&
            members.map((item) => {
              return (
                <Table.Row key={item._id}>
                  <Table.Cell align="start">{item?._id}</Table.Cell>
                  <Table.Cell align="start">
                    <Box
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        gap: "12px",
                      }}
                    >
                      {item?.avatar_url ? (
                        <Avatar
                          sx={{ width: "32px", height: "32px" }}
                          src={item.avatar_url}
                        />
                      ) : (
                        <AvatarByName
                          sx={{
                            width: "32px",
                            height: "32px",
                            fontSize: "12px",
                          }}
                          name={item.name}
                        />
                      )}
                      {item?.username}
                    </Box>
                  </Table.Cell>
                  <Table.Cell align="start">{item?.name}</Table.Cell>
                  <Table.Cell align="start">{item?.info}</Table.Cell>
                  <Table.Cell>
                    <Chip
                      color={item?.is_validation ? "success" : "error"}
                      label={item?.is_validation ? "Active" : "Not Active"}
                    />
                  </Table.Cell>
                  <Table.Cell align="start">
                    {moment(item?.createdAt).format("DD/MM/YYYY hh:mm A")}
                  </Table.Cell>
                  <Table.Cell>
                    <ButtonActionsUser />
                  </Table.Cell>
                </Table.Row>
              );
            })}
        </Table.Body>
      </Table>
      {!total && !isLoading && (
        <Typography
          sx={{ textAlign: "center", padding: "32px", fontSize: "24px" }}
        >
          No result
        </Typography>
      )}
      {!!total && (
        <Pagination
          style={{ justifyContent: "flex-end" }}
          totalItems={total}
          itemPerPage={10}
        />
      )}
    </Box>
  );
};

export default ListManageUser;
