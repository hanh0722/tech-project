import React, { ChangeEvent, FC, useEffect, useState } from "react";
import PropTypes from 'prop-types';
import { useSelector } from "react-redux";
import useCallApi from "../../../../../hook/useCallApi";
import useInput from "../../../../../hook/useInput";
import { getAllCountries } from "../../../../../service/countries";
import { RootState } from "../../../../../store";
import { RightEditAccountProps } from "../../../../../types/components/Dashboard";
import { User } from "../../../../../types/redux";
import { isFunction } from "../../../../../types/type";
import {
  isEmail,
  isMobilePhone,
  isRequired,
} from "../../../../../utils/string";
import { Button, Grid, Input } from "../../../../core";
import styles from "./styles.module.scss";

const RightEditAccount: FC<RightEditAccountProps> = (props) => {
  const { className, onGetName, onGetCountry, onGetPhoneEmail, companyRef, roleRef, isLoading, ...restProps } = props;
  const user = useSelector<RootState, User | null>((state) => state.user?.user);
  const {
    forceUpdateValue,
    isTouched,
    isValid,
    onChangeHandler,
    onTouchedHandler,
  } = useInput(null, isRequired);
  const {
    forceUpdateValue: forceUpdateEmailPhone,
    isTouched: isTouchedPhone,
    isValid: isValidPhone,
    onChangeHandler: onChangePhoneHandler,
    onTouchedHandler: onTouchedPhoneHandler,
  } = useInput("or", isMobilePhone, isEmail);

  useEffect(() => {
    forceUpdateValue(user?.name || "");
    forceUpdateEmailPhone(user?.info || "");
  }, [user]);

  const onChangeName = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    onChangeHandler(event);
    if (isFunction(onGetName)) {
      onGetName(value);
    }
  };
  const onChangeEmailPhone = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    onChangePhoneHandler(event);
    if (isFunction(onGetPhoneEmail)) {
      onGetPhoneEmail(value);
    }
  };
  return (
    <>
      <Grid cols={2}>
        <Input
          onChange={onChangeName}
          label="Full Name"
          defaultValue={user?.name}
        />
        <Input onChange={onChangeEmailPhone} defaultValue={user?.info} label="Mobile Phone / Email" />
      </Grid>
      <Grid className={styles.grid} cols={1}>
        <Input
          className={styles.input}
          readOnly
          defaultValue={user?.username}
          label="Username"
        />
      </Grid>
      <div className="d-flex align-center justify-end">
        <Button disabled={!isValidPhone || !isValid || isLoading} type="submit" variant="contained">
          Save Changes
        </Button>
      </div>
    </>
  );
};

RightEditAccount.defaultProps = {
  onGetCountry: undefined,
  onGetName: undefined,
  onGetPhoneEmail: undefined,
  companyRef: undefined,
  roleRef: undefined
};

RightEditAccount.propTypes = {
  onGetCountry: PropTypes.func,
  onGetName: PropTypes.func,
  onGetPhoneEmail: PropTypes.func,
  companyRef: PropTypes.any,
  roleRef: PropTypes.any
}

export default RightEditAccount;
