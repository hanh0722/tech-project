import React, { ChangeEvent, FC, LegacyRef, useRef, useState } from "react";
import { useSelector } from "react-redux";
import PropTypes from 'prop-types';
import { AVATAR_DEFAULT } from "../../../../../constants/default";
import useCallApi from "../../../../../hook/useCallApi";
import { uploadFile } from "../../../../../service/upload";
import { RootState } from "../../../../../store";
import { LeftEditAccountProps } from "../../../../../types/components/Dashboard";
import { User } from "../../../../../types/redux";
import { FileResponse } from "../../../../../types/request";
import { isFunction } from "../../../../../types/type";
import { classList } from "../../../../../utils/string";
import { ButtonToggle, LoadingSpinner, UploadFile } from "../../../../common";
import { Image } from "../../../../core";
import { IconPhoto } from "../../../../core/Icons";
import styles from "./styles.module.scss";
import { convertFileToUrl } from "../../../../../utils/editor";

const LeftEditAccount: FC<LeftEditAccountProps> = (props) => {
  const { onGetAvatarURL, className, ...restProps } = props;
  const fileRef = useRef<UploadFile>(null);
  const user = useSelector<RootState, User | null>((state) => state.user?.user);
  const [url, setUrl] = useState<string>(user?.avatar_url || AVATAR_DEFAULT);

  const onUploadFileHandler = (file?: Array<File>) => {
    if (file?.length === 0 || !file) {
      return;
    }
    return uploadFile([...file]);
  };
  const onCallbackFile = (data: FileResponse) => {
    const urlAvatar = data?.urls?.[0];
    setUrl(urlAvatar);
    if (isFunction(onGetAvatarURL)) {
      onGetAvatarURL(urlAvatar);
    }
  };
  const { isLoading, onSendRequest } = useCallApi<FileResponse, Array<File>>({
    request: onUploadFileHandler,
    onSuccess: onCallbackFile,
  });
  const onUploadHandler = () => {
    fileRef.current?.onClickFile();
  };
  const onGetFileHandler = async (event: ChangeEvent<HTMLInputElement>) => {
    const file = (event.target.files || []) as Array<File>;
    const firstFile = file?.[0];
    if (firstFile) {
      const url = await convertFileToUrl(firstFile) as string;
      setUrl(url);
    }
    onSendRequest(file);
  };
  return (
    <>
      <div {...restProps} className={classList(styles.edit, className)}>
        <div className="d-flex align-center justify-end">
          <div
            className={`${styles.badge} ${
              !user?.is_validation && styles.unactive
            }`}
          >
            {user?.is_validation ? "Active" : "Not Validation"}
          </div>
        </div>
        <div className="d-flex align-center justify-center">
          <div className={`d-flex align-center justify-center ${styles.flow}`}>
            <div className={styles.user}>
              <span
                onClick={onUploadHandler}
                className={`d-flex align-center justify-center flex-column ${styles.upload}`}
              >
                {isLoading ? (
                  <LoadingSpinner className={styles.loading}/>
                ) : (
                  <>
                    <IconPhoto variant="lg" />
                    <p className="f-12">Update Photo</p>
                  </>
                )}
              </span>
              <Image src={url} alt="" />
            </div>
          </div>
        </div>
        <div
          className={`d-flex align-center justify-between ${styles.validate}`}
        >
          <p className="weight-500">Email Verified</p>
          <ButtonToggle
            disabled
            className={styles.checked}
            initialStatus={user?.is_validation}
          />
        </div>
      </div>
      <UploadFile
        multiple={false}
        accepts="image/*"
        onChange={onGetFileHandler}
        ref={fileRef}
      />
    </>
  );
};

LeftEditAccount.defaultProps = {
  className: '',
  onGetAvatarURL: (url) => {}
};

LeftEditAccount.propTypes = {
  className: PropTypes.string,
  onGetAvatarURL: PropTypes.func
}

export default LeftEditAccount;
