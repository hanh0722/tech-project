import React, { FC, FormEvent, useRef, useState } from "react";
import PropTypes from "prop-types";
import styles from "./styles.module.scss";
import { ClassNameProps } from "../../../../types/string";
import { classList } from "../../../../utils/string";
import LeftEditAccount from "./LeftEditAccount";
import RightEditAccount from "./RightEditAccount";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../../store";
import { User } from "../../../../types/redux";
import { useUpdateUserProfile } from "../../../../service/profile";
import { getUserThunk } from "../../../../store/slices/user";
import { ToastNotification } from "../../../core";

const EditAccountContainer: FC<ClassNameProps> = (props) => {
  const { className, ...restProps } = props;
  const dispatch = useDispatch();
  const { mutate: onUpdateUserProfile, isLoading: isLoadingUpdateUserProfile } = useUpdateUserProfile();
  const user = useSelector<RootState, User | null>((state) => state.user?.user);
  const [avatarUser, setAvatarUser] = useState(user?.avatar_url || "");
  const [fullName, setFullName] = useState(user?.name || "");
  const [info, setInfo] = useState(user?.info || "");
  const submitFormHandler = (event: FormEvent) => {
    event.preventDefault();
    if (!fullName || !info) {
      return;
    }
    onUpdateUserProfile({
      full_name: fullName,
      info,
      avatar_url: avatarUser ?? ''
    }, {
      onSuccess: () => {
        dispatch(getUserThunk());
        ToastNotification.success("Update profile successfully");
      },
      onError: (error) => {
        const message = error instanceof Error ? error.message : 'Something went wrong while trying to update profile, please try again.'
        ToastNotification.error(message);
      }
    })
  };
  const onChangeNameHandler = (value: string) => {
    setFullName(value);
  };
  const onChangePhoneEmailHandler = (value: string) => {
    setInfo(value);
  };
  const onChangeAvatar = (avatar: string) => {
    setAvatarUser(avatar);
  }

  return (
    <form
      onSubmit={submitFormHandler}
      {...restProps}
      className={classList("d-flex", className)}
    >
      <div className={styles.container}>
        <LeftEditAccount onGetAvatarURL={onChangeAvatar}/>
      </div>
      <div className={styles.right}>
        <RightEditAccount
          isLoading={isLoadingUpdateUserProfile}
          onGetPhoneEmail={onChangePhoneEmailHandler}
          onGetName={onChangeNameHandler}
        />
      </div>
    </form>
  );
};

EditAccountContainer.defaultProps = {
  className: "",
};

EditAccountContainer.propTypes = {
  className: PropTypes.string,
};

export default EditAccountContainer;
