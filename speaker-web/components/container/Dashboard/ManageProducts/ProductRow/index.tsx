import React, { FC, useState } from "react";
import PropTypes from "prop-types";
import { useRouter } from "next/router";
import { ProductRowProps } from "../../../../../types/components/Dashboard";
import { classList, toCurrency } from "../../../../../utils/string";
import { getFullDate } from "../../../../../utils/time";
import { CheckBox } from "../../../../common";
import { Button, Image, Menu, Table } from "../../../../core";
import { IconThreeDot, IconTrash, IconTwoLine } from "../../../../core/Icons";
import IconEdit from "../../../../core/Icons/IconEdit";
import MenuItem from "../../../../core/Menu/MenuItem";
import styles from "./styles.module.scss";
import { EDIT_PRODUCT } from "../../../../../constants/path";
import { Chip } from "@mui/material";
import ArchiveProduct from "../ArchiveProduct/ArchiveProduct";
import { ProductStatus } from "../../../../../types/request";

const ProductRow: FC<ProductRowProps> = (props) => {
  const router = useRouter();
  const [showArchive, setShowArchive] = useState(false);
  const { product, className, onTick, onDelete, onRefresh, ...restProps } = props;
  const id = product?._id;
  const status = product?.status;
  const onCheck = () => {
    if (onTick) {
      onTick(id);
    }
  };
  const onHandleDelete = () => {
    if (onDelete) {
      onDelete(null, id);
    }
  };
  const onEdit = () => {
    router.push(EDIT_PRODUCT(id));
  };
  
  const onArchiveProduct = () => {
    setShowArchive(true);
  };

  const onHide = () => {
    setShowArchive(false);
  }
  return (
    <>
    <Table.Row {...restProps} className={classList(styles.row, className)}>
      <Table.Cell>
        <CheckBox onChangeCheck={onCheck} />
      </Table.Cell>
      <Table.Cell className={styles.title} align="start">
        <Image className={styles.image} src={product?.images[0]} alt="" />
        {product.title}
      </Table.Cell>
      <Table.Cell align="start">
        {getFullDate(product.creation_time)}
      </Table.Cell>
      <Table.Cell>
        <Chip
          variant="filled"
          label={product.status.toLowerCase()}
          sx={{ "& .MuiChip-label": { textTransform: "capitalize" } }}
        />
      </Table.Cell>
      <Table.Cell>{toCurrency(product.price)}</Table.Cell>
      <Table.Cell align="center">
        <Menu
          trigger={
            <Button
              variant="text"
              prefix="normal"
              color="inherit"
              className={styles.options}
            >
              <IconThreeDot />
            </Button>
          }
        >
          <MenuItem onClick={onHandleDelete}>
            <IconTrash /> Delete
          </MenuItem>
          <MenuItem onClick={onEdit}>
            <IconEdit /> Edit
          </MenuItem>
          <MenuItem onClick={onArchiveProduct}>
            <IconTwoLine /> {status === ProductStatus.ARCHIVED ? 'Unarchive' : 'Archive'}
          </MenuItem>
        </Menu>
      </Table.Cell>
    </Table.Row>
    <ArchiveProduct status={status} onSuccess={onRefresh} productId={id} open={showArchive} onClose={onHide}/>
    </>
  );
};

ProductRow.defaultProps = {
  product: undefined,
  className: "",
  onTick: (id) => {},
};

ProductRow.propTypes = {
  product: PropTypes.any.isRequired,
  className: PropTypes.string,
  onTick: PropTypes.func,
};
export default ProductRow;
