/* eslint-disable react/no-unescaped-entities */
import {
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogProps,
  Typography,
} from "@mui/material";
import { IconTrash } from "../../../../core/Icons";
import styles from "./styles.module.scss";
import useCallApi from "../../../../../hook/useCallApi";
import { updateProductStatus } from "../../../../../service/products";
import { ProductStatus } from "../../../../../types/request";
import { ToastNotification } from "../../../../core";

interface ArchiveProductProps extends DialogProps {
  productId: string;
  onSuccess?: () => void;
  status: ProductStatus;
}
const ArchiveProduct = ({
  onClose,
  productId,
  onSuccess,
  status,
  ...rest
}: ArchiveProductProps) => {
  const isArchive = status === ProductStatus.ARCHIVED;
  const onUpdateStatus = () => {
    return updateProductStatus({ productId, status: isArchive ? ProductStatus.ACTIVE : ProductStatus.ARCHIVED });
  };

  const onHandleSuccess = () => {
    onCloseHandler();
    ToastNotification.success(`${isArchive ? 'Unarchive': 'Archive'} product successfully`);
    if (onSuccess) {
      onSuccess();
    }
  };

  const onHandleError = (err: any) => {
    ToastNotification.error(
      err?.message ?? "Something went wrong while trying to archive"
    );
  };

  const { isLoading, onSendRequest } = useCallApi({
    request: onUpdateStatus,
    onSuccess: onHandleSuccess,
    onError: onHandleError,
  });
  const onCloseHandler = () => {
    if (onClose) {
      onClose({}, "backdropClick");
    }
  };
  return (
    <Dialog onClose={onCloseHandler} {...rest}>
      <Box
        sx={{
          background: "#fff",
          padding: "20px",
          minWidth: "600px",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Box
          sx={{
            width: "120px",
            height: "120px",
            borderRadius: "50%",
            background: "rgb(252, 235, 234)",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <IconTrash className={styles.icon} />
        </Box>
        <Box sx={{ textAlign: "center", paddingTop: "24px" }}>
          <Typography variant="h5" sx={{ fontWeight: "600" }}>
            {isArchive ? 'Unarchive' : 'Archive'} confirmation
          </Typography>
          <Typography sx={{ padding: "16px 0" }}>
            You can {isArchive ? 'archive' : 'unarchive'} anytime
          </Typography>
          <Button variant="outlined" onClick={onCloseHandler}>
            Cancel
          </Button>
          <Button
            disabled={isLoading}
            onClick={onSendRequest}
            sx={{ marginLeft: "16px" }}
            variant="contained"
          >
            {isLoading && <CircularProgress size={12} sx={{ color: "#fff", marginRight: '8px' }} />}
            Confirm
          </Button>
        </Box>
      </Box>
    </Dialog>
  );
};

export default ArchiveProduct;
