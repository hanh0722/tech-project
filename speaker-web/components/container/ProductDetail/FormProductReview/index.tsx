import {
  Box,
  BoxProps,
  Button,
  Rating,
  TextField,
  Typography,
} from "@mui/material";
import { ChangeEvent, FC, SyntheticEvent, useMemo, useState } from "react";
import { Dropzone, ToastNotification } from "../../../core";
import { useCreateRating } from "../../../../service/rating";

interface FormProductReviewProps extends BoxProps {
  productId: string;
  onSuccess?: (reviewId: string) => void;
}

const FormProductReview: FC<FormProductReviewProps> = (props) => {
  const { productId, onSuccess, ...rest } = props;
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [rating, setRating] = useState(0);
  const [attachments, setAttachments] = useState<string[]>([]);
  const { mutate: onCreateRating, isLoading: isLoadingCreateRating } =
    useCreateRating();

  const onChangeTitle = (event: ChangeEvent<HTMLInputElement>) => {
    setTitle(event.target.value);
  };

  const onChangeDescription = (event: ChangeEvent<HTMLInputElement>) => {
    setDescription(event.target.value);
  };

  const onGetFileHandler = (file: string[]) => {
    setAttachments(file);
  };

  const onSubmitHandler = (event: ChangeEvent<HTMLFormElement>) => {
    event.preventDefault();
    onCreateRating(
      {
        productId,
        rateNumber: rating,
        title,
        description,
        attachments: attachments.map((item) => ({ url: item })),
      },
      {
        onSuccess: (payload) => {
          ToastNotification.success("Create review successfully");
          if (onSuccess) {
            onSuccess(payload.data);
          }
        },
        onError: (err) => {
          const message =
            (err as Error)?.message ??
            "Something went wrong, please try again.";
          ToastNotification.error(message);
        },
      }
    );
  };

  const onChangeRating = (
    _: SyntheticEvent<Element, Event>,
    value: number | null
  ) => {
    setRating(value ?? 0);
  };

  const formIsValid = !!title && !!rating;
  return (
    <form onSubmit={onSubmitHandler}>
      <Box {...rest}>
        <TextField
          fullWidth
          label="Enter title"
          sx={{ marginBottom: "20px" }}
          onChange={onChangeTitle}
        />
        <TextField
          onChange={onChangeDescription}
          fullWidth
          multiline
          minRows={5}
          label="Enter review..."
        />
        <Box sx={{ display: "flex", alignItems: "center", paddingTop: "20px" }}>
          <Typography>Rating: </Typography>
          <Rating onChange={onChangeRating} sx={{ marginLeft: "12px" }} />
        </Box>
        <Box sx={{ paddingTop: "20px" }}>
          <Dropzone onGetFile={onGetFileHandler} />
        </Box>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "flex-end",
            paddingTop: "20px",
          }}
        >
          <Button
            disabled={!formIsValid || isLoadingCreateRating}
            type="submit"
            variant="contained"
          >
            Review
          </Button>
        </Box>
      </Box>
    </form>
  );
};

export default FormProductReview;
