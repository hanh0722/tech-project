import { Box, Collapse, IconButton, Typography } from "@mui/material";
import { FC, useState } from "react";
import { IconCaret } from "../../../core/Icons";
import FormProductReview from "../FormProductReview";
import ListProductReviews from "../ListProductReviews";

interface ProductReviewsProps {
  productId: string;
}
const ProductReviews: FC<ProductReviewsProps> = (props) => {
  const { productId } = props;
  const [open, setOpen] = useState(true);
  const onOpen = () => {
    setOpen((prevState) => !prevState);
  };

  const onSuccessHandler = () => {
    setOpen(false);
  }
  return (
    <Box>
      <Box
        onClick={onOpen}
        sx={{
          display: "inline-flex",
          alignItems: "center",
          paddingBottom: "20px",
          cursor: "pointer",
        }}
      >
        <Typography
          sx={{ fontWeight: "600", paddingRight: "8px" }}
          variant="h5"
        >
          Enter your review
        </Typography>
        <IconButton>
          <IconCaret
            style={{
              transition: "all .2s ease-in-out",
              transform: `rotate(${open ? "0" : "-90deg"})`,
            }}
          />
        </IconButton>
      </Box>
      <Collapse in={open} unmountOnExit>
        <FormProductReview onSuccess={onSuccessHandler} productId={productId} />
      </Collapse>
      <ListProductReviews productId={productId}/>
    </Box>
  );
};

export default ProductReviews;
