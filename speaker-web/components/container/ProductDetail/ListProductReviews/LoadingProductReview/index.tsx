import styled from "@emotion/styled";
import { Box, Skeleton } from "@mui/material";
import { generateArray } from "../../../../../utils/array";

const SkeletonElement = styled(Skeleton)`
    transform: scale(1, 1);
`;
const LoadingProductReview = () => {
  return (
    <Box>
      <SkeletonElement sx={{marginBottom: '16px'}} width={"35%"} height={30} />
      <SkeletonElement width="100%" height={80}/>
      <Box sx={{display: 'flex', alignItems: 'center', gap: '16px', paddingTop: '16px'}}>
        {generateArray(3).map(item => {
            return <SkeletonElement width={50} height={50} key={item}/>
        })}
      </Box>
    </Box>
  );
};

export default LoadingProductReview;
