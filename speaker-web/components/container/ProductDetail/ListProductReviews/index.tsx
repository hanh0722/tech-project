import { FC } from "react";
import { useGetRating } from "../../../../service/rating";
import { Box, Typography } from "@mui/material";
import LoadingProductReview from "./LoadingProductReview";
import { generateArray } from "../../../../utils/array";
import ProductReviewItem from "./ProductReviewItem";

interface ListProductReviewsProps {
  productId: string;
}
const ListProductReviews: FC<ListProductReviewsProps> = (props) => {
  const { productId } = props;
  const { data, isLoading } = useGetRating({
    productId,
  });
  const items = data?.data ?? [];
  return (
    <Box>
      <Typography variant="h5" sx={{ fontWeight: "600" }}>
        Ratings
      </Typography>
      <Box
        sx={{
          display: "grid",
          gridTemplateColumns: "repeat(1, 1fr)",
          gap: "20px",
          paddingTop: "20px",
        }}
      >
        {isLoading &&
          generateArray(5).map((item) => (
            <Box
              key={item}
              sx={{
                padding: "16px",
                borderRadius: "4px",
                border: "1px solid #ddd",
              }}
            >
              <LoadingProductReview />
            </Box>
          ))}
        {!isLoading &&
          items.length > 0 &&
          items.map((item) => {
            return <ProductReviewItem key={item._id} review={item} />;
          })}
        {!isLoading && items.length === 0 && <Typography>No reviews</Typography>}
      </Box>
    </Box>
  );
};

export default ListProductReviews;
