import { Avatar, Box, Rating as RatingItem, Typography } from "@mui/material";
import { Rating } from "../../../../../types/rating";
import { FC } from "react";
import AvatarByName from "../../../../common/AvatarByName";

interface ProductReviewItemProps {
  review: Rating;
}
const ProductReviewItem: FC<ProductReviewItemProps> = (props) => {
  const { review } = props;
  const avatarUrl = review?.user?.avatar_url;
  const name = review?.user?.username;
  const rating = review?.rateNumber ?? 0;
  const reviewTitle = review?.title;
  const description = review?.description;
  const attachments = review?.attachments ?? [];
  return (
    <Box
      sx={{ padding: "16px", borderRadius: "4px", border: "1px solid #ddd" }}
    >
      <Box sx={{ display: "flex", alignItems: "center" }}>
        {avatarUrl ? (
          <Avatar sx={styles.avatar} src={avatarUrl} />
        ) : (
          <AvatarByName sx={styles.avatar} name={name ?? ""} />
        )}
        <Box sx={{ paddingLeft: "12px" }}>
          <Typography sx={{ fontWeight: "600" }}>
            {name ?? "Anonymous"}
          </Typography>
          <RatingItem value={rating} readOnly />
        </Box>
      </Box>
      <Box sx={{ paddingTop: "16px", paddingBottom: "16px" }}>
        <Typography sx={{ fontWeight: "500", paddingBottom: "12px" }}>
          {reviewTitle}
        </Typography>
        <Typography variant="body2">{description}</Typography>
      </Box>
      {attachments.length > 0 && (
        <Box sx={{ display: "flex", alignItems: "center" }}>
          {attachments.map((item) => {
            return (
              <Avatar
                sx={{
                  ...styles.avatar,
                  borderRadius: "4px",
                  border: "1px solid #ddd",
                }}
                key={item._id}
                title={item.title}
                src={item.url}
              />
            );
          })}
        </Box>
      )}
    </Box>
  );
};

const styles = {
  avatar: {
    width: "50px",
    height: "50px",
  },
};

export default ProductReviewItem;
