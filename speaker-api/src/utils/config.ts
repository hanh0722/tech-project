import axios, { AxiosRequestConfig } from "axios";

class Request extends axios.Axios {
  constructor(config?: AxiosRequestConfig) {
    super(config);
  }
}

export default Request;