import StripeConfig from "stripe";
import { StripeProps } from "../types/stripe";
export class Stripe {
  private stripe: StripeConfig;
  constructor() {
    this.stripe = new StripeConfig(process.env["STRIPE_KEY"]!, {
      apiVersion: "2020-08-27",
      typescript: true,
    });
    this.checkout = this.checkout.bind(this);
    this.validate = this.validate.bind(this);
  }

  checkout(input: StripeProps) {
    const { cancel_url, items, mode, success_url, options } = input;
    return this.stripe.checkout.sessions.create({
      success_url: success_url,
      cancel_url: cancel_url,
      payment_method_types: ["card"],
      line_items: items.map((item) => {
        return {
          name: item.productId.title,
          amount: (item.productId.discount_price || item.productId.price) * 100,
          currency: "usd",
          quantity: item.quantity,
          images: item.productId.images,
        };
      }),
      mode: mode || "payment",
      ...(options || {}),
    });
  }

  validate(stripeKey: string) {
    return new Promise<{
      data: StripeConfig.Response<StripeConfig.Checkout.Session>;
      isSuccess: boolean;
    }>(async (resolve, reject) => {
      try {
        console.log(this.stripe);
        const response = await this.stripe.checkout.sessions.retrieve(stripeKey);
        const status = response.status;
        resolve({
          data: response,
          isSuccess: status === "complete",
        });
      } catch (err) {
        reject(err);
      }
    });
  }
}