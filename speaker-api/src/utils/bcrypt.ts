import bcrypt from "bcrypt";

class Bcrypt {
  private SALT_ROUND = 10;
  constructor(private data: string | Buffer) {
    this.hash = this.hash.bind(this);
    this.compare = this.compare.bind(this);
  }

  hash(): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      try{
        const payload = await bcrypt.hash(this.data, this.SALT_ROUND);
        resolve(payload);
      }catch(err) {
        const error = err as Error;
        const message = error?.message ?? "Server Internal Error";
        reject(message);
      }
    })
  }

  compare(encryptedString: string) {
    return new Promise<boolean>(async (resolve, reject) => {
      try{
        const result = await bcrypt.compare(this.data, encryptedString);
        resolve(result);
      }catch(err) {
        const error = err as Error;
        const message = error?.message ?? "Server Internal Error";
        reject(message);
      }
    })
  }
}

export default Bcrypt;