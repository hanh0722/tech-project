import nodemailer from "nodemailer";

const transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 587,
  secure: false,
  auth: {
    user: process.env.ADMIN_EMAIL!,
    pass: process.env.ADMIN_PASSWORD!,
  },
});

export const sendEmail = async (configMail: any) => {
  const { to, from, subject, text, html } = configMail;
  const response = await transporter.sendMail({
    from: from || process.env.ADMIN_EMAIL!,
    to,
    subject,
    html,
    text,
  });
  return response;
};
