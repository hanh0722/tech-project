import { getSocket, init } from "./config/socket";
import { useSocketMiddleWare } from "./middleware/socket";
import Router from "./models/Router";
import Cloudinary from "./models/Cloudinary";

const appRouter = new Router();
new Cloudinary();
const app = appRouter.getApp();

const server = app.listen(process.env.PORT || 9000, () => {
  console.log(`Server is running at port ${process.env.PORT || 9000}`);
});
const io = init(server);

io.on('connection', socket => {
  console.log(socket.id);
})
useSocketMiddleWare();

getSocket().on("connection", (socket) => {});
