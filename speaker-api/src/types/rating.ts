import { Types } from 'mongoose';
import { ProductProps } from "./product";

export interface IRating {
  user: Types.ObjectId | undefined;
  title: string;
  description: string;
  rateNumber: number;
  product: string | ProductProps<string[]>;
}