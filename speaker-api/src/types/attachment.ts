import { Types } from 'mongoose';
import { IRating } from './rating';
export interface IAttachment {
  url: string;
  rating: Types.ObjectId | IRating;
  title: string;
}