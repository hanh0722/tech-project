abstract class ResponseProperties {
  public code?: number;
  public message?: string;
}

abstract class BaseResponse<T> extends ResponseProperties {
  public data?: T;
}

export class ResponseEntity<T extends unknown> extends BaseResponse<T> {
  public code?: number;
  public data?: T;
  public message?: string;

  constructor(props: BaseResponse<T>) {
    super();
    const { code, data, message } = props;
    this.code = code;
    this.data = data;
    this.message = message;
  }
}

abstract class ErrorProperties<T> extends ResponseProperties {
  public errors?: T;
}

export class ErrorEntity<T extends unknown> extends ErrorProperties<T> {
  public errors?: T;
  public code?: number | undefined;
  public message?: string | undefined;

  constructor(props: ErrorProperties<T>) {
    super();
    const { code, errors, message } = props;
    this.code = code;
    this.errors = errors;
    this.message = message;
  }
}

abstract class PaginationResponseProperties<T> extends ResponseEntity<T> {
  public total?: number;
}

export class PaginationResponseEntity<T extends unknown> extends PaginationResponseProperties<T> {
  public total?: number | undefined;
  constructor({total, ...rest}: PaginationResponseProperties<T>) {
    super(rest);
    this.total = total;
  }
}