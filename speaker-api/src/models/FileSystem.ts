import fs from 'fs';

abstract class FileSystemProperties {
  abstract existFile(path: string): boolean;
  abstract removeFile(path: string, cb: (err: any) => void): void;
}

class FileSystem extends FileSystemProperties {
  constructor() {
    super();
  }
  existFile(path: string): boolean {
    return fs.existsSync(path);
  }
  removeFile(path: string, cb: (err: any) => void): void {
    const isExistedFile = this.existFile(path);
    if (!isExistedFile) {
      return;
    }
    return fs.unlink(path, (err) => cb(err));
  }
  createFolder(path: string) {
    return fs.mkdirSync(path);
  }
}

export default FileSystem;
