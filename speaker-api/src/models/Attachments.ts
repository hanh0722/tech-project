import { Schema, Types, model } from 'mongoose';
import { IAttachment } from '../types/attachment';

const Attachments = new Schema<IAttachment>({
  url: {
    type: String,
    required: true
  },
  title: {
    type: String,
    default: ""
  },
  rating: {
    type: Types.ObjectId,
    ref: 'rating',
    required: true
  }
}, {
  timestamps: true
});

export default model('attachments', Attachments);