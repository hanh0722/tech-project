import { Schema, model, Types } from 'mongoose';
import { IRating } from '../types/rating';

const Rating = new Schema<IRating>({
  user: {
    type: Types.ObjectId,
    ref: 'user',
    required: true
  },
  description: {
    type: String,
  },
  rateNumber: {
    type: Number,
    required: true,
  },
  title: {
    type: String,
    required: true,
    default: ""
  },
  product: {
    type: Types.ObjectId,
    ref: 'product',
    required: true
  }
}, {
  timestamps: true
});

export default model('rating', Rating);