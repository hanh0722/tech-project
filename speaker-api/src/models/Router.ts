import express, { Express } from "express";
import path from 'path';
import mongoose from "mongoose";
import {
  fileRouter,
  authRouter,
  productRouter,
  collectionRouter,
  cartRouter,
  countryRouter,
  addressRouter,
  checkoutRouter,
  paymentRouter,
  blogRouter,
  chatRouter,
  userRouter,
  orderRouter,
  ratingRouter,
} from "../routes";
import { handleError } from "../controller/error";
import { useCors } from "../controller/cors";
import { root } from "../utils/root";
import Multer from "./Multer";

enum Routes {
  FILE = "/api/file",
  AUTH = "/api/auth",
  PRODUCTS = "/api/products",
  COLLECTIONS = "/api/collections",
  CART = "/api/cart",
  COUNTRY = "/api/country",
  ADDRESS = "/api/address",
  CHECKOUT = "/api/checkout",
  PAYMENT = "/api/payment",
  BLOG = "/api/blog",
  CHAT = "/api/chat",
  USER = "/api/user",
  ORDERS = "/api/orders",
  RATING = "/api/rating",
}

class Router {
  private app: Express;
  private multer: Multer
  private MONGODB_USERNAME = process.env["MONGODB_USERNAME"];
  private MONGODB_PASSWORD = process.env["MONGODB_PASSWORD"];
  private MONGODB_DB_NAME = 'speaker-api';

  constructor() {
    this.app = express();
    this.config();
    this.multer = new Multer(this.app);
    this.getApp = this.getApp.bind(this);
    this.enableCors();
    this.registerRoutes();
    this.connection();
    this.handleError();
  }

  private mappingRouter() {
    return {
      [Routes.FILE]: fileRouter,
      [Routes.AUTH]: authRouter,
      [Routes.PRODUCTS]: productRouter,
      [Routes.COLLECTIONS]: collectionRouter,
      [Routes.CART]: cartRouter,
      [Routes.COUNTRY]: countryRouter,
      [Routes.ADDRESS]: addressRouter,
      [Routes.CHECKOUT]: checkoutRouter,
      [Routes.PAYMENT]: paymentRouter,
      [Routes.BLOG]: blogRouter,
      [Routes.CHAT]: chatRouter,
      [Routes.USER]: userRouter,
      [Routes.ORDERS]: orderRouter,
      [Routes.RATING]: ratingRouter,
    };
  }

  public getApp() {
    return this.app;
  }

  private registerRoutes() {
    const routes = this.mappingRouter();
    Object.entries(routes).forEach((item) => {
      const [path, controller] = item;
      this.app.use(path, controller);
    });
  }

  private connection() {
    mongoose
      .connect(
        `mongodb+srv://${this.MONGODB_USERNAME}:${this.MONGODB_PASSWORD}@cluster0.bhp9h.mongodb.net/${this.MONGODB_DB_NAME}?retryWrites=true&w=majority`
      )
      .then((result) => {
        
      });
  }

  private handleError() {
    this.app.use(handleError);
  }

  private enableCors() {
    this.app.use(useCors);
  }

  private config() {
    this.app.use(express.json());
    this.app.use(express.static(path.join(root, "static")));
    this.app.use(express.urlencoded({ extended: false }));
  }
}

export default Router;
