import { Express } from "express";
import path from 'path';
import multer from "multer";
import { randomNumber } from "../utils/string";
import { KEY_MULTER } from "../constants/key";
import FileSystem from "./FileSystem";

class Multer {
  constructor(private app: Express) {
    this.initMulter();
  }

  private storage() {
    const handleStorage = multer.diskStorage({
      destination: (req, file, cb) => {
        const fileSystem = new FileSystem();
        const pathFolder = path.join(__dirname, '../', "upload")
        const isExistedFile = fileSystem.existFile(pathFolder);
        if (!isExistedFile) {
          fileSystem.createFolder(pathFolder);
        }
        cb(null, path.join(pathFolder));
      },
      filename: (req, file, cb) => {
        randomNumber((random) => {
          cb(null, random + "-" + (file.filename || file.originalname));
        });
      },
    });
    return handleStorage;
  }

  private initMulter() {
    this.app.use(multer({storage: this.storage()}).array(KEY_MULTER));
  }
}

export default Multer;
