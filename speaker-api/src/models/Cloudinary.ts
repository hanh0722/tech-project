import { v2 } from 'cloudinary';
import FileSystem from './FileSystem';
class Cloudinary {
  constructor() {
    this.config();
  }

  config() {
    v2.config({
      cloud_name: process.env["CLOUD_NAME"],
      api_key: process.env["CLOUDINARY_KEY"],
      api_secret: process.env["CLOUDINARY_KEY_SECRET"],
      secure: true,
    });
  }
  static upload(filePath: string) {
    return new Promise((resolve, reject) => {
      const fileSystem = new FileSystem();
      v2.uploader.upload(filePath, (err, response) => {
        fileSystem.removeFile(filePath, (error) => {
          if (error || err) {
            reject("Cannot upload file");
          }
          resolve(response);
        });
      });
    });
  }
}

export default Cloudinary;
