import { Router } from "express";
import { createCollectionController, getCollectionController, getCollectionDetailController, getProductByCollection } from "../controller/collections";
import { authMiddleware } from "../middleware/auth";
import { validateUserByRole } from "../middleware/validate-user";
import { createCollectionValidate } from "../validation/collections";

const router = Router();

router.get('/get', getCollectionController);

router.post('/create', authMiddleware, createCollectionValidate, validateUserByRole,  createCollectionController);

router.get('/:id', getCollectionDetailController);

router.get('/:id/products', getProductByCollection);

export default router;