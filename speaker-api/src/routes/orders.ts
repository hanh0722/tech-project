import { Router } from "express";
import { authMiddleware } from "../middleware/auth";
import { getListOrdersController, getOrderDetailByIdController } from "../controller/orders";

const router = Router();

router.post('/list', authMiddleware, getListOrdersController);

router.get('/:id', authMiddleware, getOrderDetailByIdController);

export default router;