import { Router } from "express";
import { createRatingValidation, getRatingValidation } from "../validation/rating";
import { authMiddleware } from "../middleware/auth";
import { createRatingController, getRatingController } from "../controller/rating";

const router = Router();

router.post('/create', authMiddleware, createRatingValidation, createRatingController);

router.get('/list/:productId', getRatingValidation, getRatingController)

export default router;