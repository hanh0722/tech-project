import { RequestHandler } from "express";
import { SortBaseRequest } from "../types/base";
import { PAGE_DEFAULT, PAGE_SIZE } from "../constants/string";
import { generateRegexFindObject, generateSortObject } from "../utils/query";
import Order from "../models/Order";
import { ErrorEntity, ResponseEntity } from "../models/Response";

export const getListOrdersController: RequestHandler = async (
  req,
  res,
  next
) => {
  try {
    const {
      page = PAGE_DEFAULT,
      page_size = PAGE_SIZE,
      query,
      sort = -1,
      value,
      key = 'creation_time',
    } = req.body as SortBaseRequest;
    const sortObject = generateSortObject(key, sort);
    const findQuery = generateRegexFindObject(query, value);
    const data = await Order.find(findQuery)
      .sort(sortObject)
      .skip((+page - 1) * +page_size)
      .limit(+page_size)
      .populate("object_id", "_id name");
    const total = await Order.find(findQuery).countDocuments();

    res.json(
      new ResponseEntity({
        code: 200,
        message: "OK",
        data: {
          data,
          total,
        },
      })
    );
  } catch (err) {
    next(err);
  }
};

export const getOrderDetailByIdController: RequestHandler = async (
  req,
  res,
  next
) => {
  try {
    const { id } = req.params;
    if (!id) {
      return res.status(403).json(
        new ErrorEntity({
          code: 403,
          message: "Order id is required",
        })
      );
    }
    const order = await Order.findById(id)
      .populate({
        path: "object_id",
        select: "_id name",
      })
      .populate("object_info_id");
    if (!order) {
      return res.status(404).json(
        new ErrorEntity({
          code: 404,
          message: "Order is not existed",
        })
      );
    }
    res.json(
      new ResponseEntity({
        message: "Successfully",
        code: 200,
        data: order,
      })
    );
  } catch (err) {
    next(err);
  }
};
