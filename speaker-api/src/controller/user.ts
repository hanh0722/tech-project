import { RequestHandler } from "express";
import { validationResult } from "express-validator";
import { PAGE_DEFAULT, PAGE_SIZE } from "../constants/string";
import User from "../models/User";
import { PostUpdateUser, SearchUserQuery } from "../types/user";
import { generateRegexFindObject, generateSortObject } from "../utils/query";
import { getUserResponse } from "../utils/response";
import { ErrorEntity, ResponseEntity } from "../models/Response";

export const searchUserController: RequestHandler = async (req, res, next) => {
  const {
    page = PAGE_DEFAULT,
    page_size = PAGE_SIZE,
    query,
    sort = 'desc',
    value,
    key = 'createdAt',
    s,
  } = req.query as SearchUserQuery;
  try {
    const searchRegex = new RegExp(`.*${s || ""}.*`);
    const sortObject = generateSortObject(key, sort);
    const filterObject = generateRegexFindObject(query, value);

    const querySearchUser = {
      name: { $regex: searchRegex, $options: "i" },
      _id: { $ne: req.userId },
      ...filterObject,
    };
    const user = await User.find(querySearchUser)
      .sort(sortObject)
      .skip((+page - 1) * +page_size)
      .limit(+page_size);

    const filterDataUser = user.map((u) => getUserResponse(u));

    const totalUserMatching = await User.find(querySearchUser).countDocuments();
    res.json({
      ...new ResponseEntity({
        message: "successfully",
        code: 200,
        data: filterDataUser,
      }),
      total_users: totalUserMatching,
    });
  } catch (err) {
    next(err);
  }
};

export const updateInfoUserController: RequestHandler = async (
  req,
  res,
  next
) => {
  const { full_name, info, avatar_url } = req.body as PostUpdateUser;

  try {
    const isValidInfo = validationResult(req);
    if (!isValidInfo.isEmpty()) {
      return res.status(422).json(
        new ErrorEntity({
          code: 422,
          message: isValidInfo.array()[0].msg,
          errors: isValidInfo.array(),
        })
      );
    }
    const userId = req.userId;
    console.log(userId);
    const user = await User.findById(userId);
    console.log(user);
    if (!user) {
      return res.status(404).json(
        new ErrorEntity({
          code: 404,
          message: "User is not existed in the system",
        })
      );
    }
    user.name = full_name;
    user.info = info;
    user.avatar_url = avatar_url;
    await user.save();
    res.json(
      new ResponseEntity({
        code: 200,
        message: "Successfully",
      })
    );
  } catch (err) {
    next(err);
  }
};
