import { RequestHandler } from "express";
import { validationResult } from "express-validator";
import { PAGE_DEFAULT, PAGE_SIZE } from "../constants/string";
import Collection from "../models/Collection";
import { SortBaseRequest } from "../types/base";
import { CollectionHandler } from "../types/collections";
import { generateSlug } from "../utils/string";
import {
  ErrorEntity,
  PaginationResponseEntity,
  ResponseEntity,
} from "../models/Response";
import Product from "../models/Product";

export const getCollectionController: RequestHandler = async (
  req,
  res,
  next
) => {
  const {
    key,
    page = PAGE_DEFAULT,
    page_size = PAGE_SIZE,
    sort,
  } = req.query as SortBaseRequest;
  try {
    let objectSort = {};
    if (sort && key) {
      objectSort = {
        [key]: sort,
      };
    }
    const collections = await Collection.find({})
      .skip((+page - 1) * page_size)
      .limit(page_size)
      .sort(objectSort);
    const documents = await Collection.find({}).countDocuments();

    res.json({
      message: "successfully",
      code: 200,
      data: collections,
      total_collections: documents,
      total_page: Math.round(documents / page_size),
    });
  } catch (err) {
    next(err);
  }
};

export const createCollectionController: RequestHandler = async (
  req,
  res,
  next
) => {
  const { image_url, title } = req.body as CollectionHandler;
  try {
    const validate = validationResult(req);
    if (!validate.isEmpty()) {
      return res.status(422).json(
        new ErrorEntity({
          message: validate.array()[0].msg,
          code: 422,
          errors: validate.array(),
        })
      );
    }
    const collection = new Collection({
      image_url,
      title,
      seo_id: generateSlug(title),
    });
    const collectionAfterSave = await collection.save();
    res.json(
      new ResponseEntity({
        code: 200,
        message: "successfully",
        data: collectionAfterSave,
      })
    );
  } catch (err) {
    next(err);
  }
};

export const getCollectionDetailController: RequestHandler = async (
  req,
  res,
  next
) => {
  const { id } = req.params;
  try {
    if (!id) {
      return res.status(401).json(
        new ErrorEntity({
          code: 401,
          message: "Collection id is required",
        })
      );
    }
    const collection = await Collection.findById(id);
    if (!collection) {
      return res.status(404).json(
        new ErrorEntity({
          code: 404,
          message: "Collection is not existed",
        })
      );
    }
    res.json(
      new ResponseEntity({
        code: 200,
        message: "OK",
        data: collection,
      })
    );
  } catch (err) {
    next(err);
  }
};

export const getProductByCollection: RequestHandler = async (
  req,
  res,
  next
) => {
  const { id } = req.params;
  const { page = PAGE_DEFAULT, page_size = PAGE_SIZE } = req.query;
  try {
    if (!id) {
      return res.status(401).json(
        new ErrorEntity({
          code: 401,
          message: "Collection id is required",
        })
      );
    }
    const filterQuery = {
      collections: id,
    };
    const sortBy = {
      createdAt: "desc",
    };
    const products = await Product.find(filterQuery)
      .sort(sortBy)
      .skip((+page - 1) * +page_size)
      .limit(+page_size);
    const countDocuments = await Product.find(filterQuery).countDocuments();
    res.json(
      new PaginationResponseEntity({
        code: 200,
        message: "OK",
        data: products,
        total: countDocuments,
      })
    );
  } catch (err) {
    next(err);
  }
};
