import { ErrorRequestHandler } from "express";
import { ErrorEntity } from "../models/Response";

export const handleError: ErrorRequestHandler = (err, req, res, next) => {
  const message = err?.message || "Server Internal Error";
  const code = err?.code || 500;
  res.status(code).json(
    new ErrorEntity({
      message: message,
      code: code,
    })
  );
};
