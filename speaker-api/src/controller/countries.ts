import { RequestHandler } from "express";
import axios from 'axios';
import { isArray } from "../utils/type";
import { ErrorEntity, ResponseEntity } from "../models/Response";

export const onGetCountryController: RequestHandler = async (
  req,
  res,
  next
) => {
  try {
    const response = await axios.get(
      "https://countriesnow.space/api/v0.1/countries/states"
    );
    const data = response.data?.data;
    if (!isArray(data) || data?.length === 0) {
      return res.status(404).json(
        new ErrorEntity({
          message: "Not existed country",
          code: 404,
        })
      );
    }
    res.json(
      new ResponseEntity({
        message: "Successfully",
        code: 200,
        data: data,
      })
    );
  } catch (err) {
    next(err);
  }
};
