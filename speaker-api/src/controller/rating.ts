import { RequestHandler } from "express";
import { validationResult } from "express-validator";
import {
  ErrorEntity,
  PaginationResponseEntity,
  ResponseEntity,
} from "../models/Response";
import Attachments from "../models/Attachments";
import Rating from "../models/Rating";
import { SortBaseRequest } from "../types/base";
import { PAGE_DEFAULT, PAGE_SIZE } from "../constants/string";
import { generateRegexFindObject, generateSortObject } from "../utils/query";

export const createRatingController: RequestHandler = async (
  req,
  res,
  next
) => {
  const { title, rateNumber, productId, description, attachments } = req.body;
  const userId = req.userId ?? '';
  try {
    const validation = validationResult(req);
    if (!validation.isEmpty()) {
      return res.status(422).json(
        new ErrorEntity({
          code: 422,
          errors: validation.array(),
          message: validation.array()[0].msg,
        })
      );
    }
    const rating = new Rating({
      title,
      product: productId,
      rateNumber,
      description,
      user: userId
    });
    await rating.save();
    if (Array.isArray(attachments) && attachments.length > 0) {
      const writeData = attachments.map((item) => {
        const { url, title = "" } = item;
        return {
          insertOne: {
            document: {
              title,
              url,
              rating: rating._id,
            },
          },
        };
      });
      await Attachments.bulkWrite(writeData);
    }
    res.json(
      new ResponseEntity({
        code: 200,
        message: "Successfully",
        data: rating._id,
      })
    );
  } catch (err) {
    next(err);
  }
};

export const getRatingController: RequestHandler = async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json(
        new ErrorEntity({
          code: 422,
          message: errors.array()[0].msg,
          errors: errors.array(),
        })
      );
    }
    const { productId } = req.params;
    const {
      key = "createdAt",
      page = PAGE_DEFAULT,
      page_size = PAGE_SIZE,
      query,
      sort = "desc",
      value,
    } = req.query as SortBaseRequest;
    const findQuery = generateRegexFindObject(query, value);
    const sortQuery = generateSortObject(key, sort);
    const ratings = await Rating.find({ product: productId, ...findQuery }).populate({
      path: 'user',
      select: '_id username'
    })
      .sort(sortQuery)
      .skip((+page - 1) * +page_size)
      .limit(+page_size);
    const total = await Rating.countDocuments(findQuery);
    const ratingsWithAttachments = await ratings.reduce<any>(async (acc, payload) => {
      const data = payload.toObject();
      const attachments = await Attachments.find({rating: data._id}).select('_id url title');
      const p = await acc;
      return [...p, {...data, attachments}];
    }, []);
    res.json(
      new PaginationResponseEntity({
        code: 200,
        data: ratingsWithAttachments,
        message: "Successfully",
        total,
      })
    );
  } catch (err) {
    next(err);
  }
};
