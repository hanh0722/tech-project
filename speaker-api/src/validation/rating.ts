import { body, param } from "express-validator";
import Product from "../models/Product";

export const createRatingValidation = [
  body('title').not().isEmpty().withMessage('Title is required'),
  body('rateNumber').not().isEmpty().withMessage("Rating is required"),
  body('productId').not().isEmpty().withMessage("Product is required").custom(async (productId) => {
    try{
      const product = await Product.findById(productId);
      if (!product) {
        return Promise.reject("Product is not existed");
      }
      return true;
    }catch(err: any) {
      return Promise.reject(err?.message ?? "Server internal error");
    }
  })
]

export const getRatingValidation = [
  param('productId').not().isEmpty().withMessage('Product ID is required')
]